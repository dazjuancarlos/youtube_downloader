from pytube import YouTube


def download(link):
    yt=YouTube(link)

    res = []

    for a in yt.streams:
        # print(str(a.type))
        if str(a.type) == 'audio':
            res.append(a)

    max_a = res[0]

    for a in res:
        compare_max = int(str(max_a.abr).replace('kbps',''))
        compare_other = int(str(a.abr).replace('kbps',''))
        if compare_max < compare_other:
            max_a = a

    max_a.download('./downloads')


def read_file():
    f = open("list.txt", "r")
    return f.readlines()

for a in read_file():
    download(a)